package es.sacyl.demo.formacion.dobles.entity;

public class Item {

	private String id;

    private String name;

    private String description;

    private int priceInCents;

    private boolean isAvailable;

	public Item(String id, String name, String description, int priceInCents, boolean isAvailable) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.priceInCents = priceInCents;
        this.isAvailable = isAvailable;
    }

	public String getName() {
		return this.name;
	}

}
