package es.sacyl.demo.formacion.dobles.service;

import es.sacyl.demo.formacion.dobles.entity.Customer;

public class ExternalSystemProxy {

	public void update(Customer customer) {
		customer.setHostValue("host value");
	}

}
