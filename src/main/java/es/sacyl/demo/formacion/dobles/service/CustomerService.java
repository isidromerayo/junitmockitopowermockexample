package es.sacyl.demo.formacion.dobles.service;

import es.sacyl.demo.formacion.dobles.entity.Customer;

public class CustomerService {

	private AddressService addressService;

    
    private HostService hostService;
    
	public String getPLZAddressCombinationIncludingHostValue(Customer customer, boolean b) {
		addressService.updateExternalSystems(customer);
		hostService.expand(customer);
		String result = Integer.toString(addressService.getPLZForCustomer(customer))
                + "_"
                + addressService.getAddressForCustomer(customer)
                + "_"
                + customer.getHostValue();

		return result;
	}

}
