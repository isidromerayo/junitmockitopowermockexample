package es.sacyl.demo.formacion.dobles.service;

import org.apache.commons.lang3.StringUtils;

import es.sacyl.demo.formacion.dobles.entity.Item;
import es.sacyl.demo.formacion.dobles.repository.ItemRepository;

public class ItemService {

	private ItemRepository repository;
	
	public String getItemNameUpperCase(String string) {
		Item item = repository.findById(string);
		return StringUtils.upperCase(item.getName());
	}

	public String readItemDescription(String fileName) {
		return StaticService.readFile(fileName);
	}

}
