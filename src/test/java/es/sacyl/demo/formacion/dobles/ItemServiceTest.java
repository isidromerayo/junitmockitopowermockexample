package es.sacyl.demo.formacion.dobles;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import es.sacyl.demo.formacion.dobles.entity.Item;
import es.sacyl.demo.formacion.dobles.repository.ItemRepository;
import es.sacyl.demo.formacion.dobles.service.ItemService;
import es.sacyl.demo.formacion.dobles.service.StaticService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({StaticService.class})
public class ItemServiceTest {

	// The class under test is then annotated with the @InjectMocks annotation
    @InjectMocks
    private ItemService sut; // SUT - system under test
    
	// Mockito annotation @Mock to create the mock objects
	@Mock
    private ItemRepository itemRepository;

	@InjectMocks
    private ItemService itemService;
	
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }
    @Test
    public void getItemNameUpperCase() {
 
        //
        // Given
        //
        Item mockedItem = new Item("it1", "Item 1", "This is item 1", 2000, true);
        when(itemRepository.findById("it1")).thenReturn(mockedItem);
 
        //
        // When
        //
        String result = sut.getItemNameUpperCase("it1");
 
        //
        // Then
        //
        Mockito.verify(itemRepository, times(1)).findById("it1");
        assertThat(result, is("ITEM 1"));
    }
    @Test
    public void readItemDescriptionWithoutIOException() throws IOException {
 
        //
        // Given
        //
        String fileName = "DummyName";
 
        PowerMockito.mockStatic(StaticService.class);
        when(StaticService.readFile(fileName)).thenReturn("Dummy");
 
        //
        // When
        //
        String value = itemService.readItemDescription(fileName);
 
        //
        // Then
        //
        PowerMockito.verifyStatic(times(1));
        StaticService.readFile(fileName);
        assertThat(value, equalTo("Dummy"));
    }
}
